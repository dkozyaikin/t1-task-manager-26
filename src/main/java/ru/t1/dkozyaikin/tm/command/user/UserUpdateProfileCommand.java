package ru.t1.dkozyaikin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozyaikin.tm.enumerated.Role;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.exception.user.AccessDeniedException;
import ru.t1.dkozyaikin.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "update-profile";

    @NotNull
    public static final String DESCRIPTION = "Update user profile";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[UPDATE USER PROFILE]");
        @NotNull final String userId = getAuthService().getUserId();
        System.out.println("ENTER FIRST NAME:");
        @NotNull final String fistName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        getUserService().updateUser(userId, fistName,lastName, middleName);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
