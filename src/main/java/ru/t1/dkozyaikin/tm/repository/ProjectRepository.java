package ru.t1.dkozyaikin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozyaikin.tm.api.repository.IProjectRepository;
import ru.t1.dkozyaikin.tm.enumerated.Status;
import ru.t1.dkozyaikin.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public Project create(@NotNull String userId, @NotNull String name) {
        @NotNull Project project = new Project(name);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    ) {
        @NotNull Project project = new Project(name, description);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description,
            @NotNull Status status
    ) {
        @NotNull Project project = new Project(name, description, status);
        project.setUserId(userId);
        return add(project);
    }

}

