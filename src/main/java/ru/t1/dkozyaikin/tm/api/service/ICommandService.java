package ru.t1.dkozyaikin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozyaikin.tm.command.AbstractCommand;
import ru.t1.dkozyaikin.tm.exception.AbstractException;

import java.util.Collection;

public interface ICommandService {

    void add(@Nullable AbstractCommand command) throws AbstractException;

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name) throws AbstractException;

    @Nullable
    AbstractCommand getCommandByArgument(@Nullable String argument) throws AbstractException;

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

    void clearOutput();

}
