package ru.t1.dkozyaikin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozyaikin.tm.enumerated.Role;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable String login, @Nullable String password) throws AbstractException;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email) throws AbstractException;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role) throws AbstractException;

    @Nullable
    User findByLogin(@Nullable String login) throws AbstractException;

    @Nullable
    User findByEmail(@Nullable String email) throws AbstractException;

    @Nullable
    User removeByLogin(@Nullable String login) throws AbstractException;

    @Nullable
    User removeByEmail(@Nullable String email) throws AbstractException;

    @NotNull
    User setPassword (@Nullable String id, @Nullable String password) throws AbstractException;

    @NotNull
    User updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName) throws AbstractException;

    @NotNull
    Boolean isLoginExist(@Nullable String login) throws AbstractException;

    @NotNull
    Boolean isEmailExists(@Nullable String email) throws AbstractException;

    void lockUserByLogin(@Nullable String login) throws AbstractException;

    void unlockUserByLogin(@Nullable String login) throws AbstractException;

}
