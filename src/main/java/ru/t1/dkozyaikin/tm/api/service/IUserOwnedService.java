package ru.t1.dkozyaikin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozyaikin.tm.enumerated.Sort;
import ru.t1.dkozyaikin.tm.exception.AbstractException;
import ru.t1.dkozyaikin.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    void clear(@Nullable String userId) throws AbstractException;

    boolean existsById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @NotNull
    List<M> findAll(@Nullable String userId) throws AbstractException;

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Comparator comparator) throws AbstractException;

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Sort sort) throws AbstractException;

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @NotNull
    M findOneByIndex(@Nullable String userId, @Nullable Integer index) throws AbstractException;

    @NotNull
    Integer getSize(@Nullable String userId) throws AbstractException;

    @Nullable
    M removeById(@Nullable String userId, @Nullable String id) throws AbstractException;

    @NotNull
    M removeByIndex(@Nullable String userId, @Nullable Integer index) throws AbstractException;

    @Nullable
    M add(@Nullable String userId, @Nullable M model) throws AbstractException;

    @Nullable
    M remove(@Nullable String userId, @Nullable M model) throws AbstractException;

}
