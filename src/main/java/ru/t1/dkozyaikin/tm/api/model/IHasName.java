package ru.t1.dkozyaikin.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IHasName {

    @NotNull
    String getName();

    void setName(@NotNull String name);

}
