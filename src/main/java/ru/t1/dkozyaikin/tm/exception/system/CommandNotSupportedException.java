package ru.t1.dkozyaikin.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Argument is not supported...");
    }

    public CommandNotSupportedException(@NotNull final String command) {
        super("Error! Argument ''" + command + "'' is not supported...");
    }
}
